#include "eml_safety_enforcer/safety_check_ros_interface.h"
//#include "ca_common/shapes.h"
#include "sstream"

using namespace ca;


Eigen::Quaterniond  ca::eulerToQuat(const Eigen::Vector3d &rpy){
    using namespace Eigen;
    Quaterniond q;
    q = Eigen::AngleAxisd(rpy.x(), Vector3d::UnitX())
            * AngleAxisd(rpy.y(), Vector3d::UnitY())
        * AngleAxisd(rpy.z(), Vector3d::UnitZ());
    return q;
}

Eigen::Vector3d ca::quatToEuler(const Eigen::Quaterniond &q){
    using namespace Eigen;
    Vector3d rpy = q.toRotationMatrix().eulerAngles(0, 1, 2);
    return rpy;
}



nav_msgs::Odometry ca::StatetoOdom(ca::eml::State &state){
    nav_msgs::Odometry odo;
    odo.pose.pose.position.x = state.x;
    odo.pose.pose.position.y = state.y;
    odo.pose.pose.position.z = state.z;

    odo.twist.twist.linear.x = state.v_x;
    odo.twist.twist.linear.y = state.v_y;
    odo.twist.twist.linear.z = state.v_z;

    odo.twist.twist.angular.x =  state.v_roll;
    odo.twist.twist.angular.y =  state.v_pitch;
    odo.twist.twist.angular.z =  state.v_yaw;
    
    Eigen::Quaterniond q = eulerToQuat(Eigen::Vector3d(state.roll,state.pitch,state.yaw));
    odo.pose.pose.orientation.w = q.w();
    odo.pose.pose.orientation.x = q.x();
    odo.pose.pose.orientation.y = q.y();
    odo.pose.pose.orientation.z = q.z();

    odo.header.frame_id = state.frame;
    return odo;
}

ca::eml::State ca::OdomtoState(nav_msgs::Odometry &odo){
    ca::eml::State state;
    state.x = odo.pose.pose.position.x;
    state.y = odo.pose.pose.position.y;
    state.z = odo.pose.pose.position.z;

    state.v_x = odo.twist.twist.linear.x;
    state.v_y = odo.twist.twist.linear.y;
    state.v_z = odo.twist.twist.linear.z;

    state.v_roll = odo.twist.twist.angular.x;
    state.v_pitch = odo.twist.twist.angular.y;
    state.v_yaw = odo.twist.twist.angular.z;

    Eigen::Quaterniond q;
    q.x() = odo.pose.pose.orientation.x;
    q.y() = odo.pose.pose.orientation.y;
    q.z() = odo.pose.pose.orientation.z;
    q.w() = odo.pose.pose.orientation.w;
    Eigen::Vector3d rpy = ca::quatToEuler(q);
    state.roll = rpy.x();
    state.pitch = rpy.y();
    state.yaw = rpy.z();

    state.frame = odo.header.frame_id;
    return state;
}

void SafetyCheckROSInterface::initialize(ros::NodeHandle &n, ca::SafetyCheckBase* safetyCheck_)
{
   framesConsistent = false;
   safetycheck = safetyCheck_;
   stateSub = n.subscribe("/safety_check/odo",0, &SafetyCheckROSInterface::StateCallback, this);
   pathSub = n.subscribe("/safety_check/ip_path",0, &SafetyCheckROSInterface::TrajectoryCallback, this);
   markerArrayPub = n.advertise<visualization_msgs::MarkerArray>("trajectories", 10);
   pathPub = n.advertise<ca_nav_msgs::PathXYZVPsi>("/safety_check/op_path", 10);

   std::string param = "safety_lookahead";
   if (!n.getParam(param, safety_lookahead_)){
     ROS_ERROR_STREAM("SafetyCheckROSInterface::initialize::Could not get "<< param<< " parameter.");
     exit(-1);
    }
}

void SafetyCheckROSInterface::CheckFrameConsistency(std::string stateFrame, std::string gridFrame)
{
  std::string trajectoryFrame = stateFrame;

  if(trajectoryFrame.size()>1 && trajectoryFrame[0] =='/') trajectoryFrame.erase(trajectoryFrame.begin());
  if(stateFrame.size()>1 && stateFrame[0] =='/') stateFrame.erase(stateFrame.begin());
  if(gridFrame.size()>1 && gridFrame[0] =='/') gridFrame.erase(gridFrame.begin());

  if(trajectoryFrame.compare(stateFrame) == 0 &&
     trajectoryFrame.compare(gridFrame) == 0)
  {
    framesConsistent = true;
  }
  else
  {
    ROS_ERROR_STREAM("Frames are not consistent -- trajectoryFrame::"<<trajectoryFrame<<" -- stateFrame::"<<stateFrame
    <<" -- representationFrame::"<<gridFrame);
  }
}

void SafetyCheckROSInterface::StateCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  got_state = true;
  nav_msgs::Odometry odMsg = *msg;
  qState = OdomtoState(odMsg);
  //ROS_INFO_STREAM("odMsg->vel->x::"<<odMsg.twist.twist.linear.x<<" qState->vx::"<<qState.v_x);
}

void SafetyCheckROSInterface::TrajectoryCallback(const ca_nav_msgs::PathXYZVPsi::ConstPtr& msg){
    new_trajectory_ = true;
    waiting_for_trajectory_ = false;
    ip_traj_ = *msg;
    ROS_INFO_STREAM("Got a trajectory.");
}

ca_nav_msgs::PathXYZVPsi SafetyCheckROSInterface::EML2Path(ca::eml::Trajectory &traj){
    ca_nav_msgs::PathXYZVPsi path;
    path.header.stamp = ros::Time::now();
    path.header.frame_id = traj.frame;

    ca_nav_msgs::XYZVPsi wp;
    for(size_t i=0; i<traj.xyz.size();i++){
        wp.heading = traj.rpy[i].z();
        wp.position.x = traj.xyz[i].x();
        wp.position.y = traj.xyz[i].y();
        wp.position.z = traj.xyz[i].z();
        wp.vel = traj.v_xyz[i].norm();
        path.waypoints.push_back(wp);
    }

    return path;
}

void SafetyCheckROSInterface::process()
{
  if(!got_state){
      ROS_WARN_STREAM("SafetyROSInterface Waiting for Odometry");
      return;
  }
  visualization_msgs::Marker m;
  visualization_msgs::MarkerArray markerArray;
  double safeSpeed = 0;
  ca::eml::State local_state = qState;
  std::vector<ca::eml::Trajectory> trajectoryVector = safetycheck->abortLibrary->getEmergencyManeuverLibraryState(local_state);
  std::vector<ca::eml::Trajectory> safeTrajectoryVector = safetycheck->returnSafeVelocity(local_state,safeSpeed);
  ROS_INFO_STREAM("safeSpeed::"<<safeSpeed);
  double wanted_vel = std::sqrt(qState.v_x*qState.v_x* + qState.v_y*qState.v_y);
  if(safeSpeed<(wanted_vel-0.1)){
      ROS_ERROR_STREAM("Applying Brakes! Safe trajectory size::"<<safeTrajectoryVector.size()<<"\n");
      if(safeTrajectoryVector.size()>0){
        ca_nav_msgs::PathXYZVPsi path = EML2Path(safeTrajectoryVector[0]);
        
      }
      else{
          ROS_ERROR_STREAM("Wanted to apply brakes but no eml");
          return;
      }
  }
  //  std::vector<ca::eml::Trajectory> testVector;
//  size_t trajectoryNum = 15;
//  if(safeTrajectoryVector.size()>trajectoryNum)
//  {
//    testVector.push_back(safeTrajectoryVector[trajectoryNum]);
//  }

  safetycheck->generateMarkersSafeUnSafeTrajectories(markerArray,&safeTrajectoryVector,&trajectoryVector);

  std::stringstream ss; ss<<"Safe Speed = "<<safeSpeed<<"m/s";
  visualization_msgs::Marker textMarker;
  textMarker = markerArray.markers[0];
  textMarker.points.clear();
  nav_msgs::Odometry odo = StatetoOdom(qState);
  textMarker.pose = odo.pose.pose;
  textMarker.pose.position.x = textMarker.pose.position.x + 100.0;
  textMarker.pose.position.y = textMarker.pose.position.y + 100.0;
  textMarker.pose.position.z = textMarker.pose.position.z - 80.0;

  textMarker.ns = "Velocity";
  textMarker.text = ss.str();
  textMarker.color.r = 1.0; textMarker.color.g = 1.0; textMarker.color.b = 1.0; textMarker.color.a = 0.8;
  textMarker.scale.z = 25;
  textMarker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  markerArray.markers.push_back(textMarker);

  //qState.pose.pose.position.x = 0;
  //qState.pose.pose.position.y = 0;
  //qState.pose.pose.position.z = 0;

  //qState.pose.pose.orientation.x = 0;
  //qState.pose.pose.orientation.y = 0;
  //qState.pose.pose.orientation.z = 0;
  //qState.pose.pose.orientation.w = 1;
  ca::eml::BBox bb = safetycheck->abortLibrary->getBoundingBox(qState);
  std_msgs::ColorRGBA color_; color_.r=0.3; color_.g=0.3; color_.a=0.3;

//  Frustrum frustrum(bb.near,bb.far, color_, "/base_frame");
//  m = frustrum.returnShapeMarker();
    markerArray.markers.push_back(m);
    markerArrayPub.publish(markerArray);
}

void SafetyCheckROSInterface::safety_enforcer_process()
{
  if(!got_state){
      ROS_WARN_STREAM("SafetyROSInterface Waiting for Odometry");
      return;
  }
  visualization_msgs::Marker m;
  visualization_msgs::MarkerArray markerArray;
  double safeSpeed = 0;
  //if(qState.v_x<0) qState.v_x = 0;
  //if(qState.v_y<0) qState.v_y = 0;


  ca::eml::State lo_state = qState;

  if(new_trajectory_ && ip_traj_.waypoints.size()>0){
      double lookahead_speed = std::sqrt(qState.v_x*qState.v_x + qState.v_y*qState.v_y + qState.v_z*qState.v_z);
      double path_speed = ip_traj_.waypoints[0].vel;
      if(lookahead_speed < path_speed){
          Eigen::Vector3d pos(qState.x,qState.y,qState.z);
          ca_nav_msgs::XYZVPsi wp0 = ip_traj_.waypoints[0];
          Eigen::Vector3d dir =  Eigen::Vector3d(wp0.position.x, wp0.position.y, wp0.position.z) - pos;
          double dist = dir.norm();
          if(dist<1.0 && ip_traj_.waypoints.size()>1){
              ca_nav_msgs::XYZVPsi wp1 = ip_traj_.waypoints[1];
              dir = Eigen::Vector3d(wp1.position.x, wp1.position.y, wp1.position.z) -
                      Eigen::Vector3d(wp0.position.x, wp0.position.y, wp0.position.z);
          }
          dir.normalize();
          Eigen::Vector3d vel = dir*path_speed;
          lo_state.v_x = vel.x(); lo_state.v_y = vel.y(); lo_state.v_z = vel.z();
          ROS_WARN_STREAM("Lookahead speed::"<<lookahead_speed<<" Path speed::"<<path_speed);
          ROS_WARN_STREAM("Preventing oscillations Vel::"<<lo_state.v_x<<"::"<<lo_state.v_y<<"::"<<lo_state.v_z);
          //ROS_WARN_STREAM("Orif Vel::"<<lo_state.v_x<<"::"<<lo_state.v_y<<"::"<<lo_state.v_z);
      }
  }

  double wanted_vel = std::sqrt(lo_state.v_x*lo_state.v_x + lo_state.v_y*lo_state.v_y);

  double safety_lookahead = safety_lookahead_;

  lo_state.x = lo_state.x + lo_state.v_x*safety_lookahead;
  lo_state.y = lo_state.y + lo_state.v_y*safety_lookahead;
  lo_state.z = lo_state.z + lo_state.v_z*safety_lookahead;

  lo_state.v_x = lo_state.v_x + 0.2*lo_state.v_x;
  lo_state.v_y = lo_state.v_y + 0.2*lo_state.v_y;
  lo_state.v_z = lo_state.v_z + 0.2*lo_state.v_z;


  //std::vector<ca::eml::Trajectory> trajectoryVector = safetycheck->abortLibrary->getEmergencyManeuverLibraryState(lo_state);

  std::vector<ca::eml::Trajectory> safeTrajectoryVector = safetycheck->returnSafeVelocity(lo_state,safeSpeed);

  if(safeSpeed <=  wanted_vel){
      ROS_ERROR_STREAM("Should be applying brakes!");
      ROS_INFO_STREAM("safeSpeed::"<<safeSpeed);
      ROS_INFO_STREAM("Wanted vel::"<<wanted_vel);
      if(!waiting_for_trajectory_){
          ROS_ERROR_STREAM("Applying Brakes!");
          lo_state = qState;
          std::vector<ca::eml::Trajectory> safeTrajectoryVector = safetycheck->returnSafeVelocity(lo_state,safeSpeed);
          if(safeTrajectoryVector.size()>0 && safeSpeed > 0){
            ca_nav_msgs::PathXYZVPsi path = EML2Path(safeTrajectoryVector[0]);
            pathPub.publish(path);
            waiting_for_trajectory_ = true;
            //std::exit(-1);
          }
          else{
              ca_nav_msgs::PathXYZVPsi path;
              pathPub.publish(path);
              ROS_ERROR_STREAM("Wanted to apply brakes but no eml, so empty path published.");
              return;
              //std::exit(-1);
          }
      }
  }
  else if(new_trajectory_){
      ROS_ERROR_STREAM("New Trajectory!!");
      ROS_INFO_STREAM("safeSpeed::"<<safeSpeed);
      ROS_INFO_STREAM("Wanted vel::"<<wanted_vel);
      pathPub.publish(ip_traj_);
      new_trajectory_ = false;
      //ip_traj_.clear();
  }

  safetycheck->generateMarkersSafeUnSafeTrajectories(markerArray,&safeTrajectoryVector,NULL);

  std::stringstream ss; ss<<"Safe Speed = "<<safeSpeed<<"m/s";
  visualization_msgs::Marker textMarker;
  textMarker = markerArray.markers[0];
  textMarker.points.clear();
  nav_msgs::Odometry odo = StatetoOdom(qState);
  textMarker.pose = odo.pose.pose;
  textMarker.pose.position.x = textMarker.pose.position.x + 100.0;
  textMarker.pose.position.y = textMarker.pose.position.y + 100.0;
  textMarker.pose.position.z = textMarker.pose.position.z - 80.0;

  textMarker.ns = "Velocity";
  textMarker.text = ss.str();
  textMarker.color.r = 1.0; textMarker.color.g = 1.0; textMarker.color.b = 1.0; textMarker.color.a = 0.8;
  textMarker.scale.z = 25;
  textMarker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  markerArray.markers.push_back(textMarker);

  //qState.pose.pose.position.x = 0;
  //qState.pose.pose.position.y = 0;
  //qState.pose.pose.position.z = 0;

  //qState.pose.pose.orientation.x = 0;
  //qState.pose.pose.orientation.y = 0;
  //qState.pose.pose.orientation.z = 0;
  //qState.pose.pose.orientation.w = 1;
  ca::eml::BBox bb = safetycheck->abortLibrary->getBoundingBox(qState);
  std_msgs::ColorRGBA color_; color_.r=0.3; color_.g=0.3; color_.a=0.3;

//  Frustrum frustrum(bb.near,bb.far, color_, "/base_frame");
//  m = frustrum.returnShapeMarker();
    markerArray.markers.push_back(m);
    markerArrayPub.publish(markerArray);
}

/*ca::eml::State SafetyCheckROSInterface::ProjectedLookahead(ca_nav_msgs::PathXYZVPsi &traj, ca::eml::State &controller_lookahead, ca::eml::State &curr_pos){
  //1. check if controller lookahead is too far away then interpolate between curr_pose
  //2.
  return ca::eml::State();
}*/

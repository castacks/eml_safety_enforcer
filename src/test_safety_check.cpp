#include "eml_safety_enforcer/safety_check_ros_interface.h"
#include "emergency_maneuver_library/multirotor_emergency_maneuver_library.h"
#include "representation_interface/dummy_representation_interface.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "safety_check");
  ros::NodeHandle n("~");

  ca::representation_interface::DummyRepresentationInterface dmr;

  ca::eml::MultiRotorEmergencyManeuverLibrary hl_em;
  hl_em.setParams(n);

  ca::SafetyCheckBase safetycheck;
  safetycheck.setAbortPathLibrary(&hl_em);
  safetycheck.setRepresentationInterface(&dmr);
  safetycheck.setVelocityResolution(0.5);
  safetycheck.setConsiderOccupied(5);
  safetycheck.setConsiderUnknown(1);
  safetycheck.setUnknownFractionAllowed(0.1);
  safetycheck.setMinReccomendedSpeed(0.5);
    // TODO set all the parameters here

  ca::SafetyCheckROSInterface sfcr;

  sfcr.initialize(n,&safetycheck);

  dmr.set_frame("world");

  ros::Rate loop_rate(10);
  int count = 0;
  
  while(ros::ok())
  {
    sfcr.process();
    loop_rate.sleep();
    count++;
    ros::spinOnce();
  }
  return 0;
  
}

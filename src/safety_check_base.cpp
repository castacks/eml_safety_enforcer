#include "eml_safety_enforcer/safety_check_base.h"

using namespace ca;

std::vector<ca::eml::Trajectory> SafetyCheckBase::returnSafeTrajectories(ca::eml::State &qState)
{
  std::vector<ca::eml::Trajectory> trajectoryVector = abortLibrary->getEmergencyManeuverLibraryState(qState);
  //ROS_ERROR_STREAM("checking a trajectory of size::"<<trajectoryVector.size());
  std::vector<ca::eml::Trajectory> returnTrajectoryVector = returnSafeTrajectories(trajectoryVector);
  return returnTrajectoryVector;
};


std::vector<ca::eml::Trajectory> SafetyCheckBase::returnSafeVelocity(ca::eml::State &qState, double &velocity)
{
  //ROS_ERROR_STREAM("param_min_speed::"<<param_min_reccomended_speed);
  std::vector<ca::eml::Trajectory> returnTrajectoryVector;
  double speed = std::sqrt(qState.v_x*qState.v_x + qState.v_y*qState.v_y);
  double angle = std::atan2(qState.v_y,qState.v_x);
  //ROS_INFO_STREAM("qState::x::"<<qState.x<<" y::"<<qState.y<<" vx::"<<qState.v_x<<"vy::"<<qState.v_y);
  for(; speed > param_min_reccomended_speed ; speed = speed -  paramVResolution)
  {
    qState.v_x = speed*std::cos(angle);
    qState.v_y = speed*std::sin(angle);
    returnTrajectoryVector = returnSafeTrajectories(qState);
    //ROS_INFO_STREAM("Qspeed::"<<speed<<" Safe Trajectory Size::"<<returnTrajectoryVector.size());
    if(returnTrajectoryVector.size()>0)
      break;
    else
      returnTrajectoryVector.clear();
  }
  speed = std::max(speed,param_min_reccomended_speed);
  velocity = speed;
  return returnTrajectoryVector;
}

std::vector<ca::eml::Trajectory> SafetyCheckBase::returnSafeTrajectories(std::vector<ca::eml::Trajectory> &trajectoryVector, double numTrajectories)
{
  std::vector<ca::eml::Trajectory> returnTrajectoryVector;
  bool collided = false;
  for(unsigned int i=0;i<trajectoryVector.size();i++)
  {
    unsigned int numUnknowns = 0;
    unsigned int pointsChecked = 0;
    collided = false;
    ca::eml::Trajectory & traj_obj = trajectoryVector[i];
    //ROS_ERROR_STREAM("Trajectory#"<<i<<"trajectory_size::"<<traj_obj.xyz.size());
    for (unsigned int j=0;j<traj_obj.xyz.size();j++)
    {
      pointsChecked++;
      Eigen::Vector3d v  = traj_obj.xyz[j];

      double occupancy;
      //representation->GetValue(v,occupancy);
      //ROS_INFO_STREAM("Pos::"<<v.x()<<"::"<<v.y()<<"::"<<v.z()<<"  Occupancy::"<<occupancy);
      if(!representation->GetValue(v,occupancy))
      {
        numUnknowns++;
      }      
      else if(occupancy >= paramConsiderOccupied){
        collided = true;
      }
      else if(occupancy >= paramConsiderUnknown){
        numUnknowns++;
      }      
    }
    //ROS_ERROR_STREAM("For trajectory::"<<i<<"::"<<((double)numUnknowns/(double)pointsChecked)<<"::"<<collided);
    if(!collided && ((double)numUnknowns/(double)pointsChecked) <= paramUnknownFractionAllowed)
    {
      returnTrajectoryVector.push_back(trajectoryVector[i]);
      if(returnTrajectoryVector.size() == numTrajectories)
      {
        return returnTrajectoryVector;
      }
    }
  }
  return returnTrajectoryVector;
}

void SafetyCheckBase::generateMarkersSafeUnSafeTrajectories( visualization_msgs::MarkerArray &markerArray,
                                                            std::vector<ca::eml::Trajectory> *safeTrajectoryVector,
                                                            std::vector<ca::eml::Trajectory> *allTrajectoryVector)
{
  std_msgs::ColorRGBA c;
  c.r = 1.0; c.g = 0.0; c.b = 0.0; c.a = 0.5;
  double scale = 0.1;
  if(allTrajectoryVector!=NULL)
  {
    visualization_msgs::Marker trajectoriesMarker =
        abortLibrary->displayTrajectoryMarker((*allTrajectoryVector),c,"allTrajectories",scale);
    markerArray.markers.push_back(trajectoriesMarker);
  }

  c.r = 0.0; c.g = 0.6; c.b = 0.0; c.a = 1.0;
  scale = 0.1;
  if( safeTrajectoryVector != NULL )
  {
      //ROS_ERROR_STREAM("safe trajectories size::"<<safeTrajectoryVector->size());
    visualization_msgs::Marker safeTrajectoriesMarker =
            abortLibrary->displayTrajectoryMarker((*safeTrajectoryVector),c,"safeTrajectories",scale);
    markerArray.markers.push_back(safeTrajectoriesMarker);
  }

}

ca::eml::BBox SafetyCheckBase::getUnsafeBoundingBox(ca::eml::State &qState, bool &valid)
{
  ca::eml::BBox bbox;
  std::vector<ca::eml::Trajectory> trajectoryVector = abortLibrary->getEmergencyManeuverLibraryState(qState);
  std::vector<ca::eml::Trajectory> returnTrajectoryVector = returnSafeTrajectories(trajectoryVector,1);
  if(returnTrajectoryVector.size()>0)
  {
    valid = false;
    return bbox;
  }
  bbox = abortLibrary->getBoundingBox(trajectoryVector);
  valid = true;
  return bbox;
};

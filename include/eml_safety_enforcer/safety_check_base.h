#ifndef SAFETYCHECKBASE_H_
#define SAFETYCHECKBASE_H_
#include "emergency_maneuver_library/emergency_maneuver_library_base.h"
#include "nav_msgs/Odometry.h"
#include "visualization_msgs/MarkerArray.h"
#include "representation_interface/representation_interface.h"
#include <string>
#include <vector>

namespace ca
{
	namespace ca_ri = ca::representation_interface;
  class SafetyCheckBase
  {
  	protected:
    ca_ri::RepresentationInterface<double,3>* representation;
    public:
    ca::eml::EmergencyManeuverLibraryBase* abortLibrary;
    double paramConsiderOccupied;
    double paramConsiderUnknown;
    double paramVResolution;
    double paramUnknownFractionAllowed;
    double param_min_reccomended_speed;
    void setRepresentationInterface(ca_ri::RepresentationInterface<double,3>* representation_)
                                  {	representation = representation_;}
    void setVelocityResolution(double param){paramVResolution = param;}
    void setConsiderOccupied(double considerOccupied){paramConsiderOccupied = considerOccupied;}
    void setConsiderUnknown(double considerUnknown){paramConsiderUnknown = considerUnknown;}
    void setUnknownFractionAllowed(double param){paramUnknownFractionAllowed = param;}
    void setMinReccomendedSpeed(double param){param_min_reccomended_speed = param;}
    void setAbortPathLibrary(ca::eml::EmergencyManeuverLibraryBase* abLib){ abortLibrary = abLib;}
    virtual std::vector<ca::eml::Trajectory> returnSafeTrajectories(ca::eml::State &qState);
    virtual std::vector<ca::eml::Trajectory> returnSafeTrajectories(std::vector<ca::eml::Trajectory> &trajectoryVector,
                                                                        double numTrajectories = 0);
    virtual std::vector<ca::eml::Trajectory> returnSafeVelocity(ca::eml::State &qState,double &velocity);
    virtual void generateMarkersSafeUnSafeTrajectories(visualization_msgs::MarkerArray &markerArray,
                                                       std::vector<ca::eml::Trajectory> *safeTrajectoryVector,
                                                       std::vector<ca::eml::Trajectory> *allTrajectoryVector = NULL);
    virtual ca::eml::BBox getUnsafeBoundingBox(ca::eml::State &qState, bool &valid);
    virtual ~SafetyCheckBase(){}
  };
}
#endif

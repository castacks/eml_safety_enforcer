#ifndef ACSSAFETYCHECK_H_
#define ACSSAFETYCHECK_H_
#include "guaranteedSafePlanning/hybridAbortPathLibraryHelicopter.h"
#include "guaranteedSafePlanning/safetyCheckBase.h"
#include "nea_representation_interface/nea_obstacle_grid_representation_interface.h"
#include "acs_common/VehicleDynamicsConstraints.h"


namespace CA
{
	namespace ca_ri = ca::representation_interface;
  class acsSafetyCheck: public safetyCheckBase
  {
  	std::shared_ptr<NEA::MappingClient> mapping_client_;
  	ca_ri::NEAObstacleGridRepresentationInterface nea_obs_ri_;
    hybridAbortPathLibrary hybridAbortPathLib;
    public:
		
		void initializeGridInterface()
		{			
			isGridExternal = false;
			mapping_client_ = std::make_shared<NEA::MappingClient>();
			nea_obs_ri_.set_mapping_client(mapping_client_);
			if(!obs_grid_representation_->Initialize())
			{
				ROS_ERROR_STREAM("acsSafetyCheck::Could not initialize obstacle grid representation.");
			}
		}
				
		void setMappingClient(std::shared_ptr<NEA::MappingClient> mapping_client)
		{
			mapping_client_ = mapping_client;
			isGridExternal = true;
			nea_obs_ri_.set_mapping_client(mapping_client_);
		}
		
	  bool isGridExternal;
	  bool initialize(ros::NodeHandle &n);	  
	  void setVehicleDynamicsConstraints(const acs_common::VehicleDynamicsConstraints& msg);
	  
	  acsSafetyCheck()
	  {
	  	isGridExternal = false;
  		obs_grid_representation_ = &nea_obs_ri_;
  	};
  	
	  ~acsSafetyCheck(){};
  };
}
#endif /* ACSSAFETYCHECK_H_ */

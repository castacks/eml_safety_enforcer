#ifndef SAFETYGUARANTEESROSINTERFACE_H_
#define SAFETYGUARANTEESROSINTERFACE_H_

#include "eml_safety_enforcer/safety_check_base.h"
#include <string>
#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "ca_nav_msgs/PathXYZVPsi.h"

namespace ca
{

  Eigen::Quaterniond  eulerToQuat(const Eigen::Vector3d &rpy);
  Eigen::Vector3d quatToEuler(const Eigen::Quaterniond &q);

  nav_msgs::Odometry StatetoOdom(ca::eml::State &state);
  ca::eml::State OdomtoState(nav_msgs::Odometry &odo);

  class SafetyCheckROSInterface
  {
      ca::SafetyCheckBase* safetycheck;
      bool framesConsistent;
      ros::Subscriber stateSub;
      ros::Subscriber pathSub;
      bool got_state;
      bool waiting_for_trajectory_;
      bool new_trajectory_;
      double param_safety_lookahead_;
      ros::Publisher pathPub;
      ros::Publisher markerArrayPub;
      void CheckFrameConsistency(std::string stateFrame, std::string gridFrame);
      void StateCallback(const nav_msgs::Odometry::ConstPtr& msg);
      void TrajectoryCallback(const ca_nav_msgs::PathXYZVPsi::ConstPtr& msg);
      //ca::eml::State ProjectedLookahead(ca_nav_msgs::PathXYZVPsi &traj, eml::State &controller_lookahead);


    public:
    double safety_lookahead_;
    ca::eml::State qState;
    ca_nav_msgs::PathXYZVPsi ip_traj_;
    void safety_enforcer_process();
    void process();
    void initialize(ros::NodeHandle &n, ca::SafetyCheckBase *safetycheck_);
    ca_nav_msgs::PathXYZVPsi EML2Path(ca::eml::Trajectory &traj);
    SafetyCheckROSInterface(){
        got_state = false;
        waiting_for_trajectory_ = true;
        new_trajectory_ = false;}
  };
}

#endif
